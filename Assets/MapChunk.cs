﻿using UnityEngine;
using System.Collections;

// This class is responsible for creating a new map.
public class MapChunk
{
    struct Point
    {
        public int x, y;
    }

    // Tiles are parented to this object
    private GameObject mapChunkGameObject;
    // Keep count of map chunks
    private static int mapChunkCount = 0;

    private static int _mapGenerationId = 0;
    public enum MapType { Continental, Lakes, Coastal }
    MapType _mapType;
    private int _mapChunkSizeX, _mapChunkSizeZ;

    // Data structure stores map data. 
    // Legend: {water: 0, ground: 1}
    private int[,] mapData;

    // World Seed
    private string seed;
    private bool useRandomSeed = true;
    System.Random pseudoRandom;

    // World Fill Percentage
    [Range(0, 100)]
    private int fillPercentage;
    
    // Map data is read-only
    public int[,] MapData { get { return mapData; } }
    public int MapSizeX { get { return _mapChunkSizeX; } }
    public int MapSizeZ { get { return _mapChunkSizeZ; } }

    // Get map chunk game object
    public GameObject MapChunkGameObject { get { return mapChunkGameObject; } }

    // Constructor
    public MapChunk(int mapSizeX, int mapSizeZ, MapType mapType)
    {
        this._mapChunkSizeX = mapSizeX;
        this._mapChunkSizeZ = mapSizeZ;
        this._mapType = mapType;
        _mapGenerationId++;

        mapData = new int[mapSizeX, mapSizeZ];

        InitializeGameObject();
        InitializeWorldSeed();
        GenerateMap();
    }

    void InitializeGameObject()
    {
        mapChunkGameObject = new GameObject("MapChunk: " + mapChunkCount.ToString());
    }

    void InitializeWorldSeed()
    {
        if (useRandomSeed)
        {
            seed = System.DateTime.Now.Ticks.ToString();
        }
        pseudoRandom = new System.Random(seed.GetHashCode());
    }

    void GenerateMap()
    {
        switch (_mapType)
        {
            case MapType.Continental:
                GenerateContinental();
                break;
            case MapType.Lakes:
                GenerateIslands();
                break;
            case MapType.Coastal:
                GenerateCoastal();
                break;
            default:
                break;
        }
    }

    void GenerateContinental()
    {
        float maxRadius = (Mathf.Min(_mapChunkSizeX, _mapChunkSizeZ) / 3) * 2;
        Point continentCenter;
        continentCenter.x = _mapChunkSizeX / 2;
        continentCenter.y = _mapChunkSizeZ / 2;

        // Probability of it being land or water based on its distance from the center and maximum radius
        for (int x = 0; x < _mapChunkSizeX; x++)
        {
            for (int y = 0; y < _mapChunkSizeZ; y++)
            {
                // Add map border
                if (x == 0 || x == _mapChunkSizeX - 1 || y == 0 || y == _mapChunkSizeZ - 1)
                {
                    mapData[x, y] = 1;
                }
                else if (x == continentCenter.x && y == continentCenter.y)
                {
                    mapData[x, y] = 1;
                }
            }
        }
        // Post-processing passes to detect oceans

        // Post-processing passes to remove inner lakes

    }

    void GenerateIslands()
    {
        fillPercentage = 50;
        RandomFillPass();
        // Smooth map - neighbouring tile checks
        int smoothingPasses = 10;
        for (int i = 0; i < smoothingPasses; i++)
        {
            SmoothMap();
        }
        GenerateElevationData();
    }

    void GenerateCoastal()
    {

    }

    // Helper methods
    void RandomFillPass()
    {
        // Populate array with pseudo random values
        for (int x = 0; x < _mapChunkSizeX; x++)
        {
            for (int y = 0; y < _mapChunkSizeZ; y++)
            {
                // Add map border
                if (x == 0 || x == _mapChunkSizeX - 1 || y == 0 || y == _mapChunkSizeZ - 1)
                {
                    mapData[x, y] = 1;
                }
                // Define ground (1) and water (0)
                else
                {
                    mapData[x, y] = (pseudoRandom.Next(0, 100) < fillPercentage) ? 1 : 0;
                }
            }
        }
    }

    // Islands
    void SmoothMap()
    {
        for (int x = 0; x < _mapChunkSizeX; x++)
        {
            for (int y = 0; y < _mapChunkSizeZ; y++)
            {
                int neighbouringGroundTiles = GetSurroundingGroundCount(x, y);
                if (neighbouringGroundTiles > 4) mapData[x, y] = 1;
                else if (neighbouringGroundTiles < 4) mapData[x, y] = 0;
            }
        }
    }

    // Islands
    // Returns the number of surrounding ground tiles
    int GetSurroundingGroundCount(int gridX, int gridY)
    {
        int groundTileCount = 0;
        // Loops through 3x3 grid centered at tile gridX, gridY
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                // To prevent index out of range errors
                if (neighbourX >= 0 && neighbourX < _mapChunkSizeX && neighbourY >= 0 && neighbourY < _mapChunkSizeZ)
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        groundTileCount += mapData[neighbourX, neighbourY];
                    }
                }
                // To encourage ground tile growth around edges
                else
                {
                    groundTileCount++;
                }
            }
        }
        return groundTileCount;
    }

    // Returns the distance between the specified coordinate and the nearest tile of specified type (0 = water)
    // If tile is diagonal to origin tile, distance is still only incremented by 1 for each diagonal move.
    int DistanceToNearestTileOfType(Point coordinate, int targetType)
    {
        int distance = 0;
        // step 1: scan to find nearest water tile
        // step 2: determine the distance between water tile coordinate and original coordinate.

        for (int x = 0; x < _mapChunkSizeX; x++)
        {
            for (int y = 0; y < _mapChunkSizeZ; y++)
            {

            }
        }
        return distance;
    }

    void RandomOffset()
    {

    }

    void GenerateMoistureData()
    {

    }

    void GenerateElevationData()
    {
        Point currentPoint;

        for (int x = 0; x < _mapChunkSizeX; x++)
        {
            for (int y = 0; y < _mapChunkSizeZ; y++)
            {

                currentPoint.x = x;
                currentPoint.y = y;
                // if we are looking at a ground type
                if (mapData[x, y] == 1)
                {
                    // determine the distance to nearest tile of numerical value 0.
                    DistanceToNearestTileOfType(currentPoint, 0);
                }
            }
        }
    }


}

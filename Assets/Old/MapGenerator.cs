﻿using UnityEngine;
using System.Collections;
using System;

public class MapGenerator : MonoBehaviour {

    enum WorldType { continental = 0, islands, defaultMap }
    WorldType worldType = WorldType.continental;



    // A 2-d array of integers.
    // Basically defines a grid of integers.
    // 0 - empty tile, 1 - wall
    int[,] map;

    // How much should be filled with wall
    // This integer can only be in the range of 0-100.
    [Range(0, 100)]
    public int randomFillPercent;

    public int width;
    public int height;

    public string seed;
    public bool useRandomSeed;

    [Range(0, 5)]
    public int smoothingPasses;

    // Cellular automata algorithm
    // Initially we fill the map with random configuration of 0s and 1s, then we run smoothing iterations.

    void Start()
    {
        GenerateMap();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GenerateMap();
        }
    }

    void GenerateMap()
    {
        map = new int[width, height];
        RandomFillMap();
        // Smoothing passes (currently set to 5)
        for (int i = 0; i < smoothingPasses; i++)
        {
            SmoothMap();
        }
    }

    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = Time.time.ToString();
        }
        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                // Add walls to outside
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1;
                }
                else
                {
                    // Random.Next - returns a random integer within a specified range.
                    // If the random number is less than our initial fill percent then we want to add
                    // in a wall, if it is greater, we will leave it as a blank tile.
                    map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }
    }

    // Smoothing iterations
    void SmoothMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(x, y);
                if (neighbourWallTiles > 4) map[x, y] = 1;
                else if (neighbourWallTiles < 4) map[x, y] = 0;

            }
        }
    }

    // Returns how many neighbouring tiles are walls
    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        // Loops through 3x3 grid centered at tile gridX, gridY
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                // set boundaries on area to prevent index out of range errors
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height)
                {
                    // ignore the tile passed in
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += map[neighbourX, neighbourY];
                    }
                }
                // if this is an edge tile to encourage growth of walls around the edge of the map
                else
                {
                    wallCount++;
                }
            }
        }
        return wallCount;
    }


    void OnDrawGizmos() {
        if (map != null) {
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    // If wall color black, if empty, color white.
                    Gizmos.color = (map[x, y] == 1) ? Color.black : Color.white;
                    // Center grid
                    Vector3 pos = new Vector3(-width / 2 + x + .5f, 0, -height / 2 + y + .5f);
                    Gizmos.DrawCube(pos, Vector3.one);
                }
            }
        }
     }

 

}

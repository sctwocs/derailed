﻿using UnityEngine;
using System.Collections;

// MapGraphics class interprets tile data, tile class is reponsible for creating mesh data and assigning mesh and material to new tile.
// The MapGraphics class is responsible for interpreting the chunk data from the Map class and drawing the data to the world.
public class MapGraphics : MonoBehaviour {

    private MapChunk _mapChunk; 

    private TileType _tileType;
    private Tile tile;

	// Use this for initialization
	void Start () {
        _mapChunk = new MapChunk(60, 60, MapChunk.MapType.Lakes);
        
        if (_mapChunk.MapData != null)
        {
            for (int x = 0; x < _mapChunk.MapSizeX; x++)
            {
                for (int y = 0; y < _mapChunk.MapSizeZ; y++)
                {  
                    // TODO: Align grid to world center
                    Vector3 pos = new Vector3(-_mapChunk.MapSizeX / 2 + x + 0.5f, 0, -_mapChunk.MapSizeZ / 2 + y + 0.5f);
                    //Vector3 pos = new Vector3(x, 0, y);
                    Material mMaterial = RetrieveTileMaterial(x, y);
                    tile = new Tile(pos, mMaterial);
                    // set the parent (use getter and setter in the future) 
                    tile.SetParent(_mapChunk.MapChunkGameObject.transform);      
                }
            }
        }
	}

    // Determines what material to use for tile, material is assigned in tile class
    Material RetrieveTileMaterial(int x, int y)
    {
        switch (_mapChunk.MapData[x, y])
        {
            case 0: _tileType = TileType.Water;
                break;
            case 1: _tileType = TileType.Grass;
                break;
        }
        return Resources.Load(_tileType.ToString()) as Material;
    }
}

﻿using UnityEngine;
using System.Collections;

// Responsible for creating tile object and assigning material to it.
public class Tile {
    private Vector3 _tilePosition;
    private float _tileWidth, _tileHeight;

    private Mesh mesh;

    private GameObject tileObject;
    private static int tileCount = 0;

    private Material _mMaterial;
    //public Material MMaterial { get { return mMaterial; } set { mMaterial = value; } }

    public Tile(Vector3 pos, Material mMaterial)
    {
        this._tilePosition = pos;
        this._tileWidth = 1;
        this._tileHeight = 1;
        this._mMaterial = mMaterial;
        InitializeGameObject();
        BuildMesh();
        AssignMeshToComponents();
    }

    public Tile(Vector3 pos, int width, int height, Material mMaterial)
    {
        this._tilePosition = pos;
        this._tileWidth = width;
        this._tileHeight = height;
        this._mMaterial = mMaterial;
        InitializeGameObject();
        BuildMesh();
        AssignMeshToComponents();
    }

    // Creates new empty game object
    void InitializeGameObject()
    {
        
        // Name object
        tileObject = new GameObject(tileCount.ToString());
        // Add components
        tileObject.AddComponent<MeshFilter>();
        tileObject.AddComponent<MeshCollider>();
        tileObject.AddComponent<MeshRenderer>();
        tileObject.transform.position = _tilePosition;
        tileCount++;
    }

    // Create mesh data
    void BuildMesh()
    {
        mesh = new Mesh();
        // vertices
        Vector3[] vertices = new Vector3[] {
            // top right vertex coordinate
            new Vector3(_tilePosition.x + _tileWidth, 0, _tilePosition.z + _tileHeight),
            // bottom right vertex coordinate
            new Vector3(_tilePosition.x + _tileWidth, 0, _tilePosition.z - _tileHeight),
            // top left vertex coordinate
            new Vector3(_tilePosition.x - _tileWidth, 0, _tilePosition.z + _tileHeight),
            // bottom left vertex coordinate
            new Vector3(_tilePosition.x - _tileWidth, 0, _tilePosition.z - _tileHeight),
        };

        // normals
        Vector3[] normals = new Vector3[] {
            Vector3.up,
            Vector3.up,
            Vector3.up,
            Vector3.up,
        };

        /*
        for (int i = 0; i < vertices.Length; i++)
        {
            Debug.Log(vertices[i]);
        }*/ 
        // Debug

        Vector2[] uv = new Vector2[] {
            new Vector2(1, 1),
            new Vector2(1, 0),
            new Vector2(0, 1),
            new Vector2(0, 0),
        };

        int[] triangles = new int[] {
            0, 1, 2,
            2, 1, 3,
        };

        mesh.name = "TestMesh";
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.normals = normals;
        mesh.triangles = triangles;
    }

    // Assign mesh to filter/renderer/collider
    void AssignMeshToComponents()
    {
        MeshFilter meshFilter = tileObject.GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = tileObject.GetComponent<MeshRenderer>();
        MeshCollider meshCollider = tileObject.GetComponent<MeshCollider>();

        // Assign mesh to mesh filter component
        meshFilter.mesh = mesh;
        // Assign mesh to collider component
        meshCollider.sharedMesh = mesh;
        // Assign material to render
        meshRenderer.renderer.material = _mMaterial;
    }

    // Change the parent of this tile
    public void SetParent(Transform parent)
    {
        tileObject.transform.parent = parent;
    }
	
}
